Just install all dependencies
1. npm install
2. Import categories
3. Import products
4. Create search index
    db.products.createIndex({"name":"text"}) 

And run it:
node app.js 

Untill now My shop contains dynamic content, everything is taken from mongodb.

Features done untill now:
1.Dynamic content
2.Signup/Login with local account
3.Login with Facebook
4.Login with Google
4.1 Profile Page
5.Products review
6.Share on Facebook/Google+/Pinterest/Tweeter/Reddit/Email
7.Search products by name
8.Slider for product images
9.Pay with paypal

Features soon to be done:

1.Cart:
+Can add products in Cart
-Can't remove products from Cart
-Price for products isn't ready yet

Features planed for future:
1.Wishlist
2.Currency change
