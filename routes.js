var express = require("express");
var router = express.Router();

//require controller modules
var indexController = require("./controllers/indexController");
var firstCategoryController = require("./controllers/categoryController");
var secondCategoryController = require("./controllers/categoryController");
var thirdCategoryController = require("./controllers/categoryController");
var prodCategoryController = require("./controllers/productsController");
var accountController = require("./controllers/loginController");
var searchController = require("./controllers/searchController");
var shoppingCartController = require("./controllers/shoppingController");


router.get("/", indexController.index);
router.get("/c/:categoryId", firstCategoryController.firstCategory);
router.get("/c/:categoryId/:subCategoryId", secondCategoryController.secondCategory);
router.get("/c/:categoryId/:subCategoryId/:lastCategoryId", thirdCategoryController.thirdCategory);
router.get("/p/:productId", prodCategoryController.prodCategory);
router.get("/login", accountController.renderForm);
router.post("/login", accountController.authenticate);
router.get("/auth/facebook", accountController.authFacebook);
router.get("/auth/google", accountController.authGoogle);
router.get("/auth/google/callback", accountController.authGoogleCall);
router.get("/auth/facebook/callback", accountController.authFacebookCall);
router.get("/signup", accountController.renderSignup);
router.post("/signup", accountController.proccesSignup);
router.get("/profile", accountController.profile);
router.get("/logout", accountController.logout);
router.get("/search", searchController.search);
router.get("/cart", shoppingCartController.shoppingCart);
router.post("/cart", shoppingCartController.shoppingCartAdd);


module.exports = router;