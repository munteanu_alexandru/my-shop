// Module dependencies.
var express = require("express")
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes");
var app     = express();
var mongoose = require("mongoose");
var passport = require("passport");
var flash = require("connect-flash");

var morgan = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var session = require("express-session");

var configDB = require("./config/database.js");

//config
mongoose.connect(configDB.url);
require("./config/passport")(passport);

// All environments
app.set("port", 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use("/static", express.static(path.join(__dirname, "public")));
app.use(morgan("dev"));
app.use(cookieParser());
app.use(bodyParser());

//required for passport
app.use(session({genid: function(req) {
    return genuuid() // use UUIDs for session IDs
  },
  secret: "teranova"
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//App routes
var routes = require("./routes");
app.use("/", routes);


// Run server
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
});
