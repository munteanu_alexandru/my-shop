var dbConfig = require("../config/database");

exports.prodCategory = function(req, res) {
    var _ = require("lodash");
    var mdbClient = require("mongodb").MongoClient;

    mdbClient.connect(dbConfig.url, function(err, db) {
        var products = db.collection("products");
        //Now we work in "products". First we search products by id
        products.findOne({ id: req.params.productId }).then(function(product) {
            if (product) {
                //now we search for products images
                var largeImgs = _.filter(product.image_groups, function(o) {
                    //we return only images from view_type = "large" (see products database)
                    return o.view_type === "large";
                });
                //if any products are found, render page, else send 404
                res.render("shop_products", {
                    // lodash.js lib
                    _: _,

                    // Template data
                    title: "My Shop",
                    product: product,
                    largeImgs: largeImgs,
                    user: req.user
                });
               
            } else {
              res.send("not found");
            }

            db.close();
        });
    });
};