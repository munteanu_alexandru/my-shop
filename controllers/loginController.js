var passport = require('passport');

exports.renderForm = function(req, res) {
    var _ = require("lodash");

    res.render("login", {
        // lodash.js lib
        _: _,

        // Template data 
        title: "My Shop",
        message: req.flash('loginMessage'),
        user: req.user
    });
};

exports.authenticate = passport.authenticate('local-login', {
    successRedirect : '/',
    failureRedirect : '/login',
    failureFlash : true 
});

exports.proccesSignup = passport.authenticate('local-signup', {
    successRedirect : '/login', 
    failureRedirect : '/signup', 
    failureFlash : true 
});

exports.renderSignup = function(req, res) {
    var _ = require("lodash");

    res.render("signup", {
        // lodash.js lib
        _: _,

        // Template data 
        title: "My Shop",
        message: req.flash('signupMessage'),
        user: req.user
    });
};

exports.profile = function(req, res) {
    var _ = require("lodash");

    res.render("profile", {
        // lodash.js lib
        _: _,

        // Template data
        title: "My Shop",
        user: req.user
    });       
};

exports.logout = function(req, res) {
    req.logout();
    res.redirect("/");
};

exports.authFacebook = passport.authenticate('facebook', { scope : 'email' });
exports.authFacebookCall = passport.authenticate('facebook', {successRedirect : '/',failureRedirect : '/',});

exports.authGoogle = passport.authenticate('google', { scope : ['profile', 'email'] });
exports.authGoogleCall = passport.authenticate('google', {successRedirect : '/',failureRedirect : '/'});