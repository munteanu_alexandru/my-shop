var dbConfig = require("../config/database");

exports.search = function (req, res) {
    var _ = require("lodash");
    var mdbClient = require("mongodb").MongoClient;

    mdbClient.connect(dbConfig.url, function(err, db) {
        var products = db.collection("products");
        products.find({$text: {$search: req.query.q}}, {score: {$meta: "textScore"}}).sort({score:{$meta:"textScore"}}).toArray(function(err, results) {
            
            res.render("search", {
                // lodash.js lib
                _: _,
                
                // Template data
                title: "My Shop",
                user: req.user,
                results: results
            });
        });

    });

}