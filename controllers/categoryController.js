var dbConfig = require("../config/database");

exports.firstCategory = function(req, res) {
    var _ = require("lodash");
    var mdbClient = require("mongodb").MongoClient;

    mdbClient.connect(dbConfig.url, function(err, db) {
        var collection = db.collection("categories");
        // now we find items with conditions
        // in our case we search for all categories by id and add them in array.  ex: main category mens=>find all his subcategories
        collection.find({ id: req.params.categoryId }).toArray(function(err, items) {                
            res.render("shop_firstcategory", {
                // lodash.js lib
                _: _,

                // Template data
                title: "My Shop",
                items: items,
                user: req.user
            });
            db.close();
        });
    });
};

exports.secondCategory = function(req, res) {
    var _ = require("lodash");
    var mdbClient = require("mongodb").MongoClient;

    mdbClient.connect(dbConfig.url, function(err, db) {
        
        var collection = db.collection("categories");
        // search for all items with id from main category then pass it to function      
        collection.findOne({ id: req.params.categoryId }).then(function(topCategory) {
            // if our search return with objects make another searh for his subcategories
            if (topCategory) { 
                var subCategory = _.find(topCategory.categories, function(o) {
                    //then retur only id's that match with our request
                    return o.id === req.params.subCategoryId;
                });
                //if we get any results in function from above, render page, if not, display 404 page.
                if (subCategory) {
                    res.render("shop_secondcategory", {
                        // lodash.js lib
                        _: _,

                        // Template data
                        title: "My Shop",
                        subCategory: subCategory,
                        user: req.user
                    });
                }; 
            };
            db.close();
        });
    });
};

exports.thirdCategory = function(req, res) {
    var _ = require("lodash");
    var mdbClient = require("mongodb").MongoClient;
    
     
    mdbClient.connect(dbConfig.url, function(err, db) {
        var collection = db.collection("categories");
        var products = db.collection("products");
        // search for all items with id from main category then pass it to function 
        collection.findOne({ id: req.params.categoryId }).then(function(topCategory) {
            // if our search return with objects make another searh for his subcategories
            if (topCategory) {
                var subCategory = _.find(topCategory.categories, function(o) {
                    //then retur only id's that match with our request
                    return o.id === req.params.subCategoryId;
                });
                //using the same logic as we used upper, we search under categories we got before
                if (subCategory) {
                    var lastCategory = _.find(subCategory.categories,function(o) {
                        //then retur only id's that match with our request
                        return o.id === req.params.lastCategoryId;
                    });
                    //now we search for products in collection "products" that have same id with our request
                    products.find({primary_category_id: req.params.lastCategoryId}).toArray(function(err, items) {
                        //if we find a match, add items into page
                        if (lastCategory) {
                            res.render("shop_thirdcategory", {
                                // lodash.js lib
                                _: _,

                                // Template data
                                title: "My Shop",
                                lastCategory: lastCategory,
                                items: items,
                                subCategory: subCategory,
                                user: req.user
                            });
                        };
                    });
                }; 
            }; 
            db.close();
        });
    });
};



