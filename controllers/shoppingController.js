exports.shoppingCart = function(req, res) {
    var _ = require("lodash");
    if (!req.session.cart) {
        req.session.cart = {};
    }
    res.render("cart", {
        // lodash.js lib
        _: _,
        title: "My Shop",
        cart: req.session.cart,
        user: req.user
          
    });
};

exports.shoppingCartAdd = function(req, res) {
    var _ = require("lodash");
    
    if (!req.session.cart) {
        req.session.cart = {};
    }
    if (_.has(req.session.cart, req.body.productId)) {
        req.session.cart[req.body.productId]++;
    }else {
        req.session.cart[req.body.productId] = 1;
    }

    res.send(JSON.stringify({"status": "OK"}));
};  
