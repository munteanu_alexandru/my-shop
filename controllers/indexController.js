var dbConfig = require("../config/database");

exports.index = function(req, res) {
    var _ = require("lodash");
    var mdbClient = require("mongodb").MongoClient;

    mdbClient.connect(dbConfig.url, function(err, db) {
        var collection = db.collection("categories");
        // first, we need to find first items from categories, so we use .find without any conditions
        // it will return Men and Womens (check database to understand easyer)
        collection.find().toArray(function(err, items) {
            res.render("index", {
                // lodash.js lib
                _: _,

                // Template data
                title: "My Shop",
                items: items,
                user: req.user //!Important for login to work, don't edit this.
                
            });

            db.close();
        });
    });
};
